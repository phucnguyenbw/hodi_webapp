<?php 
/**
 * config
 * 
 * @package Sngine
 * @author Zamblek
 */

// ** MySQL settings ** //
/** The name of the database */
define('DB_NAME', 'beforeusa_sngine');

/** MySQL database username */
define('DB_USER', 'elon_american');

/** MySQL database password */
define('DB_PASSWORD', 'elon_american');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** MySQL port */
define('DB_PORT', '3306');


// ** System URL ** //
define('SYS_URL', 'http://localhost:6001'); // e.g (http://example.com)


// ** i18n settings ** //
define('DEFAULT_LOCALE', 'en_us');


/**
 * For developers: Sngine debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use debug
 * in their development environments.
 */
define('DEBUGGING', false);


// ** LICENCE ** //
/**
 * A unique key for your licence.
 */
define('LICENCE_KEY', '');

?>